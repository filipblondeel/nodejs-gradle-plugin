[[npm-defaults]]
== Configure global NPM defaults

The location of `npm-cli.js` can be supplied in four possible ways:

.build.gradle
[source,groovy]
----
include::{testdir}/NpmExtensionSpec.groovy[tags=configure-with-path,indent=0]

include::{testdir}/NpmExtensionSpec.groovy[tags=configure-with-search-path,indent=0]
----
<1> Supply a path to the `npm` executable. This works for people that have `npm` installed in predefined locations.
<2> Tell Gradle to look for `npm` (or `npm.cmd`) in the system search path, then infer location of `npm-cli.js`.

NOTE: Gradle does not use the `npm` or `npm.cmd` scripts. It uses `npm-cli.js` directly.

By default, the default local configuration is always set to an `npmrc` file located in the root of the root project and the global configuration is set to `~/.gradle/npmrc`.
This is specifically done so that configuration can be set on a project-wide basis for a Gradle project without interference from a possible globally installed Node.js.
Placement of global configuration in the Gradle User Home allows a person to set specific global options for Node projects that are built by Gradle.
The location of the global and local `npmrc` files can also be set.

.build.gradle
[source,groovy]
----
npm {
  localConfig = "${projectDir}/npmrc2" // <1>
  globalConfig = "${project.rootProject.projectDir}/npmrc2" // <2>
  homeDirectory = 'src/node' // <3>

  localConfig = useLocalUserProfileLocation() // <4>
  localConfig = useLocalProjectLocation() // <5>

  globalConfig = useGlobalSystemLocation() // <6>
  globalConfig = useGlobalGradleLocation() // <7>
}
----
<1> Set location of local configuration file.
<2> Set location of global configuration file.
<3> Set the location of the Node project code. This is the directory into which `node_modules` will be generated into and per convention the parent directory of the actual Node.js source directories.
<4> Set the local configuration file to the user's configuration file.
<5> Set the local configuration file to the project directory.
<6> Set the global configuration file to use the one from the logbal installation. If the file cannot be located, the build will fail.
<7> Set the global configuration file to be in the Gradle home directory. This is also the default behaviour.

IMPORTANT: If  <<nodedev,Node.js project development>> plugin is applied, then `localConfig = useLocalUserProfileLocation()`.