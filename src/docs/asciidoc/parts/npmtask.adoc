[[npmtask]]
== NpmTask type

`NpmTask` is a generic task type for running NPM commands. In its simplest form it takes an `npm` command and a collection or arguments.

[source,groovy]
----
task createPackageJson( type : org.ysb33r.gradle.nodejs.tasks.NpmTask ) {
    command 'init' // <1>
    cmdArgs '-f', '-y' // <2>
}
----
<1> `npm` command
<2> Arguments that are applicable to the specific command

=== Customisation

By default an `NpmTask` will obtain the location of `node` from the <<node-defaults,nodejs project extension>> and any NPM configuration from the <<npm-defaults,npm project extension>>. However, this plugin allows you to have a lot of flexibility should you need it. Therefore you can override any of the settings in both of the aforementioned extensions in task extensions by the same name. This allows you to for instance run a specific task with a different version of `node` than the global configured version.

By taking the same previous example, you can do

[source,groovy]
----
task createPackageJson( type : org.ysb33r.gradle.nodejs.tasks.NpmTask ) {
    command 'init'
    cmdArgs '-f', '-y'

    nodejs {
      executable version : '7.10.0' // <1>
    }

    npm {
      localConfig "${projectDir}/npmrc2" // <2>
    }
}
----
<1> Change the specific of `node` when executing this task.
<2> Use a different local configuration file.

NOTE: This kind of flexibility is not applicable for probably the majority of cases, but there are specific use cases where this is extremely helpful.

Any other parameters that you would expect to see in an Gradle `ExecSpec` can be used as well.
