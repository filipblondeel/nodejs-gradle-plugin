/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification
import org.ysb33r.gradle.nodejs.plugins.NodeJSBasePlugin

class NpmExtensionSpec extends UnittestBaseSpecification {

    def 'Use default NPM version'() {
        expect:
        project.npm.resolvedNpmCliJs != null
    }

//    @PendingFeature
    def 'Configure NPM executable version'() {
        when: 'A tag is configured'
        project.allprojects {
            npm {
                executableByVersion('4.5.0')
            }
        }

        then:
        project.npm.resolvedNpmCliJs != null
    }

    def 'Configure NPM executable using a path'() {
        when: 'A tag is configured'
        project.allprojects {
            // tag::configure-with-path[]
            npm {
                executableByPath('/path/to/npm') // <1>
            }
            // end::configure-with-path[]
        }

        then:
        project.npm.resolvedNpmCliJs != null
    }

    def 'Configure NPM executable using a search path'() {
        when: 'A tag is configured'
        project.allprojects {
            apply plugin: NodeJSBasePlugin

            // tag::configure-with-search-path[]
            npm {
                executableBySearchPath('npm') // <2>
            }
            // end::configure-with-search-path[]
        }

        then:
        project.npm.resolvedNpmCliJs != null
    }
}