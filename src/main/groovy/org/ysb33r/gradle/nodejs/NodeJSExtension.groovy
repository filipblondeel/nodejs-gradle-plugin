/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.Transformer
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.errors.BadNvmRcException
import org.ysb33r.gradle.nodejs.internal.Downloader
import org.ysb33r.gradle.nodejs.internal.NodeJSExecSpecInstantiator
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ConfigurationException
import org.ysb33r.grolifant.api.v4.MapUtils
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.runnable.AbstractToolExtension
import org.ysb33r.grolifant.api.v4.runnable.ExecUtils
import org.ysb33r.grolifant.api.v4.runnable.ExecutableDownloader
import org.ysb33r.grolifant.api.v4.runnable.ProvisionedExecMethods

import java.util.concurrent.Callable

import static org.ysb33r.gradle.nodejs.utils.NodeJSExecutor.defaultEnvironment
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** Configure project defaults or task specifics for Node.js.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
class NodeJSExtension extends AbstractToolExtension<NodeJSExtension>
    implements ProvisionedExecMethods<NodeJSExecSpec> {

    public static final String NAME = 'nodejs'

    /** The default version of Node.js that will be used on
     * a supported platform if nothing else is configured.
     */
    public static final String NODEJS_DEFAULT = '16.14.2'

    /** Constructs a new extension which is attached to the provided project.
     *
     * @param project Project this extension is associated with.
     */
    NodeJSExtension(Project project) {
        super(ProjectOperations.find(project))
        executableByVersion(NODEJS_DEFAULT)
        this.env = [:]
        this.env.putAll(defaultEnvironment)

        prefixPath(executable.map { File it ->
            it.parentFile.canonicalPath
        })

        this.npmCliProvider = linkNpmCli()
        this.instantiator = new NodeJSExecSpecInstantiator(this, projectOperations)
        this.toolDownloader = new Downloader(projectOperations)
        this.locateNpm = {
            ExtensionContainer exts -> exts.getByType(NpmExtension)
        }.curry(project.extensions) as Callable<NpmExtension>
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     */
    NodeJSExtension(Task task) {
        super(task, ProjectOperations.find(task.project), task.project.extensions.getByType(NodeJSExtension))
        this.env = [:]
        this.npmCliProvider = linkNpmCli()
        this.instantiator = new NodeJSExecSpecInstantiator(this, projectOperations)
        this.toolDownloader = new Downloader(projectOperations)
        this.locateNpm = {
            ExtensionContainer exts -> exts.getByType(NpmExtension)
        }.curry(task.extensions) as Callable<NpmExtension>
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     * @param alternativeProjectExt Alternative extension to use, rather than the default project extension
     */
    NodeJSExtension(Task task, NodeJSExtension alternativeProjectExt) {
        super(task, ProjectOperations.find(task.project), alternativeProjectExt)
        this.env = [:]
        this.npmCliProvider = linkNpmCli()
        this.instantiator = new NodeJSExecSpecInstantiator(this, projectOperations)
        this.toolDownloader = new Downloader(projectOperations)
        this.locateNpm = {
            ExtensionContainer exts -> exts.getByType(NpmExtension)
        }.curry(task.extensions) as Callable<NpmExtension>
    }

    /**
     * Read the version from {@code .nvmrc}.
     * Only supports fixed versions, not open versions like {@code node} or {@code lts/*}.
     *
     * Assumes {@code .nvmrc} is at the {@code npm} homedirectory
     *
     * @since 0.11
     */
    void executableByNvmrc() {
        try {
            executableByNvmRc(locateNpm.call().homeDirectoryProvider.map { new File(it, '.nvmrc') })
        } catch (UnknownDomainObjectException e) {
            throw new BadNvmRcException(
                "No NpmExtension was found. This is probably due to 'org.ysb33r.nodejs.npm' plugin not installed.",
                e
            )
        }
    }

    /**
     * Read the version from {@code .nvmrc}.
     * Only supports fixed versions, not open versions like {@code node} or {@code lts/*}.
     *
     * @param nvmrc Provider to the location of {@code .nvmrc}
     *
     * @since 0.11
     */
    @SuppressWarnings('CatchArrayIndexOutOfBoundsException')
    void executableByNvmRc(Provider<File> nvmrc) {
        executableByVersion(nvmrc.map { File rc ->
            try {
                String versionLine = rc.readLines()[0]?.trim()
                if (!versionLine?.matches(~/^\d+\.\d+\.\d+.*$/)) {
                    throw new BadNvmRcException(
                        "'${versionLine}' is not an acceptable version. Gradle only accepts fixed versions."
                    )
                }
                versionLine
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new BadNvmRcException("${rc.absolutePath} seems to be empty", e)
            }
        })
    }
    /**
     * Create execution specification.
     *
     * @return Execution specification.
     *
     * @since 0.10
     */
    @Override
    NodeJSExecSpec createExecSpec() {
        instantiator.create()
    }

    /** Resolves a path to a {@code npm} executable that is associated with the configured Node.js.
     *
     * @return Returns the path to the located {@code npm} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     *
     * @since 0.10
     */
    Provider<File> getNpmCliJsProvider() {
        this.npmCliProvider
    }

    /** Replace current environment with new one.
     * If this is called on the task extension, no project extension environment will
     * be used.
     *
     * @param args New environment key-value map of properties.
     *
     * @since 0.7
     */
    void setEnvironment(Map<String, ?> args) {
        if (task) {
            taskOnlyEnvironment = true
        }
        this.env.clear()
        this.env.putAll((Map<String, Object>) args)
    }

    /** Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     *
     * @since 0.7
     */
    Map<String, String> getEnvironment() {
        if (task && taskOnlyEnvironment) {
            MapUtils.stringizeValues(this.env)
        } else if (task) {
            projExt.environment + MapUtils.stringizeValues(this.env)
        } else {
            MapUtils.stringizeValues(this.env)
        }
    }

    /** Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     *
     * @since 0.7
     */
    void environment(Map<String, ?> args) {
        this.env.putAll((Map<String, Object>) args)
    }

    /** Adds the system path to the execution environment.
     *
     * @since 0.7
     */
    void useSystemPath() {
        appendPath(projectOperations.environmentVariable(OS.pathVar, projectOperations.atConfigurationTime()))
    }

    /** Add search to system path
     *
     * In the case of a task this will be prefixed to both the task and project extension's version
     * of the system path.
     *
     * @param prefix Provider of a path item that can be prefixed to the current system path.
     *
     * @since 0.9.2
     */
    void prefixPath(Provider<String> prefix) {
        Object current = this.env[OS.pathVar]
        if (task && taskOnlyEnvironment) {
            if (current == null) {
                this.env[OS.pathVar] = prefix
            } else {
                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
            }
        } else if (task) {
            Provider<String> combinator
            NodeJSExtension parent = projExt
            if (current == null) {
                combinator = prefix.map({ String s ->
                    "${s}${OS.pathSeparator}${stringize(parent.env[OS.pathVar])}"
                } as Transformer<String, String>)
            } else {
                combinator = prefix.map({ String s ->
                    "${s}${OS.pathSeparator}${stringize(current)}"
                } as Transformer<String, String>)
            }
            this.env[OS.pathVar] = combinator
        } else {
            if (current == null) {
                this.env[OS.pathVar] = prefix
            } else {
                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
            }
        }
    }

    /** Add search to system path.
     *
     * In the case of a task this will be appended to both the task and project extension's version
     * of the system path.
     *
     * @param postfix Provider of a path item that can be appended to the current system path.
     *
     * @since 0.9.2
     */
    void appendPath(Provider<String> postfix) {
        Object current = this.env[OS.pathVar]
        if (task && taskOnlyEnvironment) {
            if (current == null) {
                this.env[OS.pathVar] = postfix
            } else {
                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
            }
        } else if (task) {
            Provider<String> combinator
            NodeJSExtension parent = projExt
            if (current == null) {
                combinator = postfix.map({ String s ->
                    "${stringize(parent.env[OS.pathVar])}${OS.pathSeparator}${s}"
                } as Transformer<String, String>)
            } else {
                combinator = postfix.map({ String s ->
                    "${stringize(current)}${OS.pathSeparator}${s}"
                } as Transformer<String, String>)
            }
            this.env[OS.pathVar] = combinator
        } else {
            if (current == null) {
                this.env[OS.pathVar] = postfix
            } else {
                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
            }
        }
    }

    /** Resolves a path to a {@code node} executable.
     *
     * @return Returns the path to the located {@code node} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     *
     * @deprecated
     */
    @Deprecated
    ResolvableExecutable getResolvableNodeExecutable() {
        NodeJSExtension proxy = this
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                proxy.executable.get()
            }
        }
    }

    /** Resolves a path to a {@code npm} executable that is associated with the configured Node.js.
     *
     * @return Returns the path to the located {@code npm} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     * @deprecated
     */
    @Deprecated
    ResolvableExecutable getResolvedNpmCliJs() {
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                npmCliProvider.get()
            }
        }
    }

    /** Use this to configure a system path search for Node
     *
     * @deprecated
     */
    @Deprecated
    static Map<String, Object> searchPath() {
        this.SEARCH_PATH
    }

    @SuppressWarnings('DuplicateStringLiteral')
    @Deprecated
    void executable(Map<String, Object> opts) {
        if (opts.containsKey('version')) {
            log.warn("'${this.class.name}#executable version' is deprecated. Use executableByVersion()")
            executableByVersion(opts['version'])
        } else if (opts.containsKey('path')) {
            log.warn("'${this.class.name}#executable path' is deprecated. Use executableByPath()")
            executableByPath(opts['path'])
        } else if (opts.containsKey('search')) {
            log.warn("'${this.class.name}#executable searchPath()' is deprecated. Use executableBySearchPath()")
            executableBySearchPath('your-exec-name') // <1>
        }
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NodeJSExtension implementation
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected NodeJSExtension(Task task, final String projectExtName) {
        super(
            task,
            ProjectOperations.find(task.project),
            (NodeJSExtension) task.project.extensions.getByName(projectExtName)
        )
        this.npmCliProvider = linkNpmCli()
        this.instantiator = new NodeJSExecSpecInstantiator(this, projectOperations)
        this.toolDownloader = new Downloader(projectOperations)
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NodeJSExtension implementation
     * @param project Project this extension is associated with.
     * @param extClass Type of the extension that is attached to the project.
     *
     * @since 0.10
     */
    protected NodeJSExtension(Task task, final Class<? super NodeJSExtension> extClass) {
        super(
            task,
            ProjectOperations.find(task.project),
            (NodeJSExtension) task.project.extensions.getByType(extClass)
        )
        this.npmCliProvider = linkNpmCli()
        this.instantiator = new NodeJSExecSpecInstantiator(this, projectOperations)
        this.toolDownloader = new Downloader(projectOperations)
    }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link ExecUtils#parseVersionFromOutput} as a helper to implement this method.
     *
     * @return Version string.
     * @throws ConfigurationException if configuration is not correct or version could not be determined.
     *
     * @since 0.10
     */
    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        ExecUtils.parseVersionFromOutput(
            projectOperations,
            ['-v'],
            executable.get(),
            { String output ->
                output.readLines()[0].replaceFirst('v', '')
            }) { ExecSpec spec ->
            spec.environment(NodeJSExecutor.defaultEnvironment)
        }
    }

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     * @since 0.10
     */
    @Override
    protected ExecutableDownloader getDownloader() {
        toolDownloader.executableDownloader
    }

    private NodeJSExtension getProjExt() {
        (NodeJSExtension) projectExtension
    }

    private Provider<File> linkNpmCli() {
        executable.map { File exe ->
            File root
            if (OS.windows) {
                root = exe.parentFile
            } else {
                root = new File(exe.parentFile.parentFile, 'lib')
            }
            new File(root, 'node_modules/npm/bin/npm-cli.js')
        }
    }

    @SuppressWarnings('UnnecessaryCast')
    @Deprecated
    private static final Map<String, Object> SEARCH_PATH = [search: 'node'] as Map<String, Object>
    private static final OperatingSystem OS = OperatingSystem.current()

    private final Map<String, Object> env = [:]
    private boolean taskOnlyEnvironment = false
    private final Provider<File> npmCliProvider
    private final NodeJSExecSpecInstantiator instantiator
    private final Downloader toolDownloader
    private final Callable<NpmExtension> locateNpm
}
