/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.exec.ExternalExecutable
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecCommandSpec

/** Specification for running an NPM command via {@code npm-cli.js}
 *
 * <p> For simplicity Gradle executes {@code npm-cli.js} directly rather
 * than use yet another indirection of the {@code npm} shell script.
 *
 * @since 0.1
 */
@CompileStatic
class NpmExecSpec extends AbstractExecCommandSpec<NpmExecSpec> {

    /** Construct class and attach it to specific project.
     *
     * @param project Project this exec spec is attached.
     * @param registry The registry to use to resolve NPM location.
     * @deprecated
     */
    @Deprecated
    NpmExecSpec(Project project, ExternalExecutable registry) {
        super(ProjectOperations.find(project))
    }

    /** Construct class and attach it to specific project.
     *
     * @param projectOperations Project this exec spec is attached.
     *
     * @since 1.0
     */
    NpmExecSpec(ProjectOperations projectOperations) {
        super(projectOperations)
    }

    /** Install a resolver to find the {@code node} executable.
     *
     * @param resolver
     */
    void setNodeExecutable(Provider<File> resolver) {
        this.nodeExecutable = resolver
    }

    /** Install a resolver to find the {@code node} executable.
     *
     * @param resolver
     */
    void nodeExecutable(Provider<File> resolver) {
        setNodeExecutable(resolver)
    }

    /**
     * Returns the arguments for the executable to be executed.
     *
     * @return Arguments resolved to strings
     *
     * @since 0.10
     */
    @Override
    Provider<List<String>> getExeArgs() {
        projectOperations.providerTools.map(super.exeArgs) { List<String> it ->
            it + ['--scripts-prepend-node-path=true']
        }
    }

    /** Builds up the command-line.
     *
     * @return list of built-up arguments.
     */
    @Override
    protected List<String> buildCommandLine() {
        [nodeExecutable.get().absolutePath] + super.buildCommandLine()
    }

    private Provider<File> nodeExecutable
}
