/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.internal.npm.CustomNpmInstaller
import org.ysb33r.gradle.nodejs.internal.npm.NpmExecSpecInstantiator
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ConfigurationException
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.runnable.AbstractToolExtension
import org.ysb33r.grolifant.api.v4.runnable.ExecUtils
import org.ysb33r.grolifant.api.v4.runnable.ExecutableDownloader
import org.ysb33r.grolifant.api.v4.runnable.ProvisionedExecMethods

/** Set up global config or task-based config for NPM.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
class NpmExtension extends AbstractToolExtension<NpmExtension> implements ProvisionedExecMethods<NpmExecSpec> {

    static final String NAME = 'npm'

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     */
    NpmExtension(Project project) {
        super(ProjectOperations.find(project))
        this.nodeJsProjectExtension = project.extensions.getByType(NodeJSExtension)
        this.homeDirectoryProperty = project.objects.property(File)
        this.homeDirectoryProperty.set(project.projectDir)
        this.instantiator = new NpmExecSpecInstantiator(this.nodeJsProjectExtension, this)
        executableByPath(nodeJsProjectExtension.npmCliJsProvider)
        this.npxProvider = executable.map { File it ->
            new File(it.parentFile, NPX_FILENAME)
        }
        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.localConfig = useLocalProjectLocation()
        this.globalConfig = useGlobalGradleLocation()

        this.npmInstaller = new CustomNpmInstaller(projectOperations, this.nodeJsProjectExtension, this)
    }

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     * @param nodejs Alternative NodeJS extension to use.
     *
     * @since 0.10
     */
    NpmExtension(Project project, NodeJSExtension nodejs) {
        super(ProjectOperations.find(project))
        this.nodeJsProjectExtension = nodejs
        this.homeDirectoryProperty = project.objects.property(File)
        this.homeDirectoryProperty.set(project.projectDir)
        this.instantiator = new NpmExecSpecInstantiator(nodejs, this)
        executableByPath(nodeJsProjectExtension.npmCliJsProvider)
        this.npxProvider = executable.map { File it -> new File(it.parentFile, NPX_FILENAME) }

        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.localConfig = useLocalProjectLocation()
        this.globalConfig = useGlobalGradleLocation()

        this.npmInstaller = new CustomNpmInstaller(projectOperations, this.nodeJsProjectExtension, this)
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     */
    NpmExtension(Task task) {
        super(
            task,
            ProjectOperations.find(task.project),
            task.project.extensions.getByType(NpmExtension)
        )
        this.nodeJsProjectExtension = task.project.extensions.getByType(NodeJSExtension)
        this.homeDirectoryProperty = task.project.objects.property(File)
        this.homeDirectoryProperty.set(npmExtension.homeDirectoryProvider)
        this.instantiator = new NpmExecSpecInstantiator(task.project.extensions.getByType(NodeJSExtension), this)
        this.npxProvider = executable.map { File it -> new File(it.parentFile, NPX_FILENAME) }

        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            task.project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.npmInstaller = new CustomNpmInstaller(projectOperations, this.nodeJsProjectExtension, this)
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     * @param nodejs Alternative NodeJS extension to use as project extension.
     * @param altNpmExt Alternative Npm extension to use as project extension.
     *
     * @since 0.10
     */
    NpmExtension(Task task, NodeJSExtension nodejs, NpmExtension altNpmExt) {
        super(
            task,
            ProjectOperations.find(task.project),
            altNpmExt
        )
        this.nodeJsProjectExtension = nodejs
        this.homeDirectoryProperty = task.project.objects.property(File)
        this.homeDirectoryProperty.set(npmExtension.homeDirectoryProvider)
        this.instantiator = new NpmExecSpecInstantiator(nodejs, this)
        this.npxProvider = executable.map { File it -> new File(it.parentFile, NPX_FILENAME) }

        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            task.project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.npmInstaller = new CustomNpmInstaller(projectOperations, nodejs, this)
    }

    /**
     * Use the NPM that is bundled with Node.
     *
     * This is the default and should raely be required.
     *
     * @since 0.12
     */
    void executableIsBundled() {
        executableByPath(nodeJsProjectExtension.npmCliJsProvider)
    }

    /**
     * The location of NPX.
     *
     * @return A provider to the location of NPX
     *
     * @since 0.10
     */
    Provider<File> getNpxCliJsProvider() {
        this.npxProvider
    }

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     *
     * @since 0.10
     */
    @Override
    NpmExecSpec createExecSpec() {
        instantiator.create(projectOperations)
    }

    /** Resolves a path to a {@code npm-cli.js} executable.
     *
     * <p> If the extension is linked to a task and not the location not configured,
     * a lookup will be performed on the project extension of the same name. This is an alias
     * method for {@code getResolvableExecutable( )}
     *
     * @return Returns the path to the located {@code npm-cli.js} executable.
     * @throw {@code ExecConfigurationException} if location was not configured.
     *
     * @deprecated
     */
    @Deprecated
    ResolvableExecutable getResolvedNpmCliJs() {
        NpmExtension proxy = this
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                proxy.executable.get()
            }
        }
    }

    /** Resolves a path to a {@code npx-cli.js} executable.
     *
     * <p> If the extension is linked to a task and not the location not configured,
     * a lookup will be performed on the project extension of the same name. This is an alias
     * method for {@code getResolvableExecutable( )}
     *
     * @return Returns the path to the located {@code npx-cli.js} executable.
     * @throw {@code ExecConfigurationException} if location was not configured.
     *
     * @since 0.9.0*
     * @deprecated
     */
    @Deprecated
    ResolvableExecutable getResolvedNpxCliJs() {
        NpmExtension proxy = this
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                new File(proxy.executable.get().parentFile, NPX_FILENAME)
            }
        }
    }

    /** Sets NPM to be resolved from the default node.js distribution associated with this project.
     *
     * @return Something that is suitable to be passed to @{@link #executable}
     *
     * @deprecated
     */
    @SuppressWarnings('UnnecessaryCast')
    @Deprecated
    Map<String, Object> defaultNodejs() {
        ['default': nodeJsProjectExtension] as Map<String, Object>
    }

    /** Use this to configure a system path search for {@code npm}
     *
     * @return Returns a special option to be used in {@link #executable}
     *
     * @deprecated
     */
    @Deprecated
    Map<String, Object> searchPath() {
        SEARCH_PATH
    }

    @Deprecated
    @SuppressWarnings(['LineLength', 'DuplicateStringLiteral'])
    void executable(Map<String, Object> opts) {
        if (opts.containsKey('version')) {
            log.warn("'${this.class.name}#executable version' is deprecated. Use executableByVersion()")
            executableByVersion(opts['version'])
        } else if (opts.containsKey('path')) {
            log.warn("'${this.class.name}#executable path' is deprecated. Use executableByPath()")
            executableByPath(opts['path'])
        } else if (opts.containsKey('search')) {
            log.warn("'${this.class.name}#executable searchPath()' is deprecated. Use executableBySearchPath()")
            executableBySearchPath('your-exec-name') // <1>
        } else if (opts.containsKey('default')) {
            log.warn("'${this.class.name}#executable default' is deprecated. Use executableByPath(nodejs.npmCliJsProvider)")
            executableByPath(nodeJsProjectExtension.npmCliJsProvider)
        }
    }

    /** Location & name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to global NPM config
     */
    File getGlobalConfig() {
        if (task) {
            this.globalConfig ? projectOperations.file(this.globalConfig) : npmExtension.getGlobalConfig()
        } else {
            projectOperations.file(this.globalConfig)
        }
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setGlobalConfig(Object path) {
        this.globalConfig = path
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void globalConfig(Object path) {
        setGlobalConfig(path)
    }

    /** Location & name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to local NPM config
     */
    File getLocalConfig() {
        if (task) {
            this.localConfig ? projectOperations.file(this.localConfig) : npmExtension.getLocalConfig()
        } else {
            projectOperations.file(this.localConfig)
        }
    }

    /**
     * Location of {@code .npmrc} for a project.
     *
     * @return Project config level.
     */
    Provider<File> getProjectConfig() {
        npmrcProviders.project
    }

    /** Set local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setLocalConfig(Object path) {
        this.localConfig = path
    }

    /** Set local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void localConfig(Object path) {
        setLocalConfig(path)
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     */
    File getHomeDirectory() {
        homeDirectoryProvider.get()
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     * @since 0.8.0
     */
    Provider<File> getHomeDirectoryProvider() {
        this.homeDirectoryProperty
    }

    /** Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     */
    void setHomeDirectory(Object homeDir) {
        projectOperations.updateFileProperty(this.homeDirectoryProperty, homeDir)
    }

    /** Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     */
    void homeDirectory(Object homeDir) {
        homeDirectory = homeDir
    }

    /**
     * Tries to determine the location where global config will typically be places.
     *
     * @return A provider to a system location.
     *
     * @since 0.12
     */
    Provider<File> useGlobalSystemLocation() {
        npmrcProviders.system
    }

    /**
     * Sets it to use the equivalent of {@code getGradleHomeDir()/.npmrc}.
     *
     * This allows Gradle projects to be custom configured on local system without being affected by any system-wide
     * node installations.
     *
     * @return Location of a global configuration file for NPM that is located inside the Gradle home directory.
     *
     * @since 0.12
     */
    Provider<File> useGlobalGradleLocation() {
        npmrcProviders.gradle
    }

    /**
     * Use to indicate that userconfig should be retrieved from the user's home directory.
     *
     * Typically this is {@code ~/.npmrc} or {@code %APPDATA\npm\.npmrc}
     *
     * @return Location for a userconfig {@code .npmrc}
     *
     * @since 0.12
     */
    Provider<File> useLocalUserProfileLocation() {
        npmrcProviders.user
    }

    /**
     * Use to indicate that userconfig should be retrieved from the NPM home directory as configured by
     * {@link #getHomeDirectory()}.
     *
     * @return Location for a userconfig {@code .npmrc}
     *
     * @since 0.12
     */
    Provider<File> useLocalProjectLocation() {
        projectConfig
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NpmExtension implementation
     *
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected NpmExtension(Task task, final String projectExtName) {
        super(
            task,
            ProjectOperations.find(task.project),
            (NpmExtension) task.project.extensions.getByName(projectExtName)
        )
        this.nodeJsProjectExtension = task.project.extensions.getByType(NodeJSExtension)
        this.instantiator = new NpmExecSpecInstantiator(this.nodeJsProjectExtension, this)
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NpmExtension implementation
     * @param Task Task this extension is associated with.
     * @param extClass Type of the extension that is attached to the project.
     *
     * @since 0.10
     */
    protected NpmExtension(Task task, final Class<? super NpmExtension> extClass) {
        super(
            task,
            ProjectOperations.find(task.project),
            (NpmExtension) task.project.extensions.getByType(extClass)
        )
        this.nodeJsProjectExtension = task.project.extensions.getByType(NodeJSExtension)
        def nodejs = task.extensions.findByType(NodeJSExtension) ?: task.extensions.getByType(NodeJSExtension)
        this.instantiator = new NpmExecSpecInstantiator(nodejs, this)
    }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link ExecUtils#parseVersionFromOutput} as a helper to implement this method.
     *
     * @return Version string.
     * @throws ConfigurationException if configuration is not correct or version could not be determined.
     *
     * @since 0.10
     */
    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        ExecUtils.parseVersionFromOutput(
            projectOperations,
            [executable.get().absolutePath, '-v'],
            nodeJsProjectExtension.executable.get(),
            { String content ->
                content.readLines()[0].trim()
            }) { ExecSpec spec ->
            spec.environment = NodeJSExecutor.defaultEnvironment
            spec.environment([
                npm_config_userconfig  : localConfig.absolutePath,
                npm_config_globalconfig: globalConfig.absolutePath
            ])
        }
    }

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     * @since 0.10
     */
    @Override
    protected ExecutableDownloader getDownloader() {
        this.npmInstaller
    }

    /** Returns the name of the extension to look for when trying to find the Node.js extension.
     *
     * @return Name of Node.js project extension
     *
     * @since 0.4
     */
    protected String getNodeJsExtensionName() {
        NodeJSExtension.NAME
    }

    /** Returns the name of the extension to look for when trying to find the Npm extension.
     *
     * @return Name of NPM project extension or (@code null) if this a project extension.
     *
     * @since 0.4
     */
    protected String getNpmProjectExtensionName() {
        this.npmProjectExtensionName
    }

    private NpmExtension getNpmExtension() {
        ((NpmExtension) projectExtension)
    }

    private static class NpmrcProviders {
        final Provider<File> system
        final Provider<File> gradle
        final Provider<File> user
        final Provider<File> project

        NpmrcProviders(
            ProjectOperations po,
            File gradleUserHomeDir,
            Provider<File> npmHomeProvider
        ) {
            File userLocation = new File(gradleUserHomeDir, '.npmrc')
            boolean windows = OperatingSystem.current().windows
            Provider<String> dataEnvVar = po.environmentVariable(
                windows ? 'APPDATA' : 'HOME',
                po.atConfigurationTime()
            )

            gradle = po.provider { -> userLocation }

            user = po.providerTools.map(dataEnvVar) { String envValue ->
                windows ? new File(envValue, "npm/${NPM_USER_CONFIG}") : new File(envValue, NPM_USER_CONFIG)
            }

            system = windows ? po.providerTools.map(dataEnvVar) { String envValue ->
                new File(envValue, "Roaming/npm/${NPM_CONFIG}")
            } : po.provider { ->
                String atDir = NPMRC_SEARCH_LOCATIONS.find {
                    new File(it, NPM_CONFIG).exists()
                }

                if (!atDir) {
                    throw new FileNotFoundException(
                        "Global ${NPM_CONFIG} not found in any of ${NPMRC_SEARCH_LOCATIONS}"
                    )
                }

                new File(atDir, NPM_CONFIG)
            }

            project = po.providerTools.map(npmHomeProvider) { File it -> new File(it, NPM_USER_CONFIG) }
        }
    }

    private Object localConfig
    private Object globalConfig
    private final Property<File> homeDirectoryProperty
    private final NodeJSExtension nodeJsProjectExtension
    private final Provider<File> npxProvider
    private final NpmExecSpecInstantiator instantiator
    private final NpmrcProviders npmrcProviders
    private final CustomNpmInstaller npmInstaller

    @SuppressWarnings(['UnnecessaryCast', 'DuplicateStringLiteral'])
    @Deprecated
    private static final Map<String, Object> SEARCH_PATH = [search: 'npm'] as Map<String, Object>

    private static final String NPM_CONFIG = 'npmrc'
    private static final String NPM_USER_CONFIG = '.npmrc'
    private static final String NPX_FILENAME = 'npx-cli.js'
    private static final List<String> NPMRC_SEARCH_LOCATIONS = [
        '/etc',
        '/usr/local/etc',
        '/etc/npm',
        '/usr/local/etc/npm'
    ]
}