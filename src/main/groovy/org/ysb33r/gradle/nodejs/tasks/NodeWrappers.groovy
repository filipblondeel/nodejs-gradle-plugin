/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.MapUtils
import org.ysb33r.grolifant.api.v4.wrapper.script.AbstractScriptWrapperTask
import org.ysb33r.grolifant.api.v5.FileUtils

import javax.inject.Inject

@CompileStatic
class NodeWrappers extends AbstractScriptWrapperTask {

    public static final String NODE_WRAPPER = 'nodew'
    public static final String NODE_WRAPPER_WINDOWS = 'nodew.bat'

    @Inject
    NodeWrappers(TaskProvider<NodeBinariesCacheTask> cacheTask) {
        super()
        ProjectOperations po = ProjectOperations.find(project)
        this.cacheTask = cacheTask
        dependsOn(cacheTask)
        inputs.file(cacheTask.map { NodeBinariesCacheTask t -> t.locationPropertiesFile })
        this.npmExt = project.extensions.getByType(NpmExtension)
        useWrapperTemplatesInResources(
            '/gradle-node-wrappers',
            MAPPING
        )
        outputs.files(Transform.toList((Collection) MAPPING.values()) { String it ->
            wrapperDestinationDirProvider.map { File f -> new File(f, it) }
        })

        this.tokens = [
            APP_BASE_NAME               : 'node',
            GRADLE_WRAPPER_RELATIVE_PATH: wrapperDestinationDirProvider.map {
                po.fsOperations.relativePathToRootDir(it) ?: CURRENT_DIR
            },
            DOT_GRADLE_RELATIVE_PATH    : wrapperDestinationDirProvider.map {
                FileUtils.relativize(it, po.projectCacheDir) ?: CURRENT_DIR
            },
            APP_LOCATION_FILE           : cacheTask.map { it.locationPropertiesFile.get().name },
            CACHE_TASK_NAME             : project.absoluteProjectPath(cacheTask.name),
        ]

        this.nodeWrapperProvider = po.providerTools.map(wrapperDestinationDirProvider) {
            OperatingSystem.current().windows ? new File(it, NODE_WRAPPER_WINDOWS) : new File(it, NODE_WRAPPER)
        }
    }

    @Internal
    Provider<File> getNodeWrapperProvider() {
        this.nodeWrapperProvider
    }

    @Override
    protected String getBeginToken() {
        '~~'
    }

    @Override
    protected String getEndToken() {
        '~~'
    }

    @Override
    protected Map<String, String> getTokenValuesAsMap() {
        MapUtils.stringizeValues(this.tokens)
    }

    private final Map<String, ?> tokens
    private final Provider<File> nodeWrapperProvider

    private static final Map<String, String> MAPPING = [
        'node-wrapper-template.sh' : NODE_WRAPPER,
        'node-wrapper-template.bat': NODE_WRAPPER_WINDOWS,
        'npm-wrapper-template.sh'  : 'npmw',
        'npm-wrapper-template.bat' : 'npmw.bat',
        'npx-wrapper-template.sh'  : 'npxw',
        'npx-wrapper-template.bat' : 'npxw.bat'
    ]

    private final TaskProvider<NodeBinariesCacheTask> cacheTask
    private final NpmExtension npmExt
    private static final String CURRENT_DIR = '.'
}
