/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Transformer
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatName
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatVersion
import static org.ysb33r.gradle.nodejs.plugins.NpmDevPlugin.DEFAULT_GROUP

/** Create a {@code package.json} file if it does not exist.
 *
 * @since 0.8.0
 */
@CompileStatic
class NpmPackageJsonInit extends AbstractNodeBaseTask {

    @Input
    String getProjectName() {
        this.projectName
    }

    @Input
    Provider<String> getVersionProvider() {
        this.versionProvider
    }

    NpmPackageJsonInit() {
        super()
        group = DEFAULT_GROUP
        this.packageJson = project.objects.property(File)
        projectOperations.updateFileProperty(
            this.packageJson, npmExtension.homeDirectoryProvider.map { File file ->
            new File(file, PACKAGE_JSON)
        })
        this.versionProvider = projectOperations.versionProvider.map({ String it ->
            formatVersion(it, true)
        } as Transformer<String, String>)
        this.projectName = formatName(project.name)
    }

    /** The package.json file that this task will create update.
     *
     * @return File object.
     */
    @Internal
    File getPackageJsonFile() {
        packageJsonFileProvider.get()
    }

    /** The package.json file that this task will create update.
     *
     * @return File object.
     */
    @OutputFile
    Provider<File> getPackageJsonFileProvider() {
        this.packageJson
    }

    @TaskAction
    void exec() {
        File packageFile = packageJsonFileProvider.get()
        if (!packageFile.exists()) {
            npmExecutor.initPkgJson(projectName, versionProvider)
        }
    }

    private final Property<File> packageJson
    private final String projectName
    private final Provider<String> versionProvider
}
