/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.npm.NpmExecSpecInstantiator
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecCommandTask

/** Ability to execute any NPM command with parameters.
 *
 * <p> The task contains an NPM extension, which by default is setup to look at the
 * global NPM extension. It allows overriding on a per-task basis of NPM configuration.
 *
 * @since 0.1
 */
@CompileStatic
class NpmTask extends AbstractExecCommandTask<NpmTask> {

    NpmTask() {
        super()
        npmExtension = (NpmExtension) (extensions.create(NpmExtension.NAME, NpmExtension, this))
        nodeExtension = (NodeJSExtension) (extensions.create(NodeJSExtension.NAME, NodeJSExtension, this))
        instantiator = new NpmExecSpecInstantiator(nodeExtension, npmExtension)
        executable = npmExtension.executable
        workingDir(npmExtension.homeDirectoryProvider)
    }

    /**
     * The environment variables to use for the process.
     *
     * @return Environment, but not yet resolved from original values.
     *
     * @since 0.10
     */
    @Override
    Map<String, Object> getEnvironment() {
        Map<String, Object> finalEnv = NpmExecutor.environmentFromExtensions(nodeExtension, npmExtension)
        finalEnv.putAll(super.environment)
        finalEnv
    }

    @Override
    protected List<String> buildCommandLine() {
        NpmExecSpec spec = instantiator.create(projectOperations)
        NpmExecutor.configureSpecFromExtensions(spec, nodeExtension, npmExtension)
        spec.command(command)
        spec.cmdArgs(this.cmdArgs.get())
        spec.commandLine
    }

    @Internal
    protected NpmExtension getNpm() {
        npmExtension
    }

    @Internal
    protected NodeJSExtension getNodejs() {
        nodeExtension
    }

    private final NpmExtension npmExtension
    private final NodeJSExtension nodeExtension
    private final NpmExecSpecInstantiator instantiator
}
