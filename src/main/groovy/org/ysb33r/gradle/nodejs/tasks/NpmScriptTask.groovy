/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal

/**
 * Runs scripts that are defined in {@code package.json}.
 *
 * @since 0.9.0
 */
@CompileStatic
class NpmScriptTask extends NpmTask {
    NpmScriptTask() {
        super()
        super.setCommand('run')
    }

    @Internal
    String scriptName

    /**
     * Set the command to use.
     *
     * @param cmd Anything that can be resolved via {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize(Object)}
     * @return {@code this}
     */
    @Override
    NpmTask setCommand(Object cmd) {
        notSupported(COMMAND_IS_FIXED)
    }

    /**
     * Any arguments specific to the command in use
     *
     * @return Arguments to the command. Can be empty, but never null.
     */
    @Override
    Provider<List<String>> getCmdArgs() {
        super.cmdArgs.map { List<String> cmds ->
            if (scriptName == null) {
                throw new GradleException('scriptName cannot be null')
            }
            cmds.empty ? [scriptName] : ([scriptName, '--'] + cmds)
        }
    }

    private void notSupported(String text) {
        throw new UnsupportedOperationException(text)
    }

    private final static String COMMAND_IS_FIXED = 'Cannot override the NPM command. Use a normal NpmTask instead.'
}
