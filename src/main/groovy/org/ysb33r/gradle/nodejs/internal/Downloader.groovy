/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.file.CopySpec
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.downloader.AbstractDistributionInstaller
import org.ysb33r.grolifant.api.v4.runnable.ExecutableDownloader

import static org.ysb33r.grolifant.api.core.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant.api.core.OperatingSystem.Arch.X86_64

/** Downloads specific versions of NodeJS.
 * Currently limited to Windows (x86, x86_64), MacOS, Linux (x86, x86_64).
 *
 * <p> There are more
 * binary packages are available from the NodeJS site, but currently these are not being tested not implemented.
 * This includes:
 *
 * <ul>
 *    <li> linux-armv6l.tar.xz
 *    <li> linux-armv7l.tar.xz
 *    <li> linux-arm64.tar.xz
 *    <li> linux-ppc64le.tar.xz
 *    <li> linux-ppc64.tar.xz
 *    <li> linux-s390x.tar.xz
 * </ul>
 * <p> (Patches welcome!)
 */
@CompileStatic
@Slf4j
class Downloader extends AbstractDistributionInstaller {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final OperatingSystem.Arch ARCH = OS.arch
    static String baseURI = System.getProperty('org.ysb33r.gradle.nodejs.uri') ?: 'https://nodejs.org/dist'

    final ExecutableDownloader executableDownloader

    /**
     * Creates a Node downloader.
     *
     * @param projectOperations Project operations context.
     *
     * @since 0.10
     */
    Downloader(final ProjectOperations projectOperations) {
        super('nodejs', 'native-binaries/nodejs', projectOperations)

        this.executableDownloader = new ExecutableDownloader() {
            File getByVersion(String version) {
                getNodeExecutablePath(version)
            }
        }
    }

    /** Provides an appropriate URI to download a specific tag of NodeJS.
     *
     * @param ver Version of Node to download
     * @return URI for a supported platform; {@code null} otherwise.
     */
    @Override
    URI uriFromVersion(final String ver) {
        String variant
        if (OS.windows) {
            if (OS.arch == X86) {
                variant = 'win-x86.zip'
            } else {
                variant = 'win-x64.zip'
            }
        } else if (OS.linux) {
            String arch
            switch (ARCH) {
                case X86_64:
                    arch = 'x64'
                    break
                case X86:
                    arch = 'x86'
                    break
            }
            if (arch) {
                variant = "linux-${arch}.tar.xz"
            }
        } else if (OS.macOsX) {
            variant = 'darwin-x64.tar.gz'
        }

        variant ? "${baseURI}/v${ver}/node-v${ver}-${variant}".toURI() : null
    }

    /** Returns the path to the {@code node} executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code node} or null if not a supported operating system.
     */
    File getNodeExecutablePath(String version) {
        getDistributionFile(version, OS.windows ? 'node.exe' : 'bin/node').getOrNull()
    }

    /** Returns the path to the included {@code npm-cli.js} Node.js executable JavaScript.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code npm-cli.js} or null if not a supported operating system.
     */
    @SuppressWarnings('UnnecessaryGetter')
    File getNpmExecutablePath(String version) {
        getDistributionFile(
            version,
            OS.windows ? 'node_modules/npm/bin/npm-cli.js' : '../lib/node_modules/npm/bin/npm-cli.js'
        ).getOrNull()
    }

    /** Validates that the unpacked distribution is good.
     *
     * <p> The default implementation simply checks that only one directory should exist and then uses that.
     * You should override this method if your distribution in question does not follow the common practice of one
     * top-level directory.
     *
     * @param distDir Directory where distribution was unpacked to.
     * @return The directory where the real distribution now exists. In the default implementation it will be
     *   the single directory that exists below {@code distDir}.
     *
     * @throw DistributionFailedException if distribution failed to
     *   meet criteria.
     */
    @Override
    protected File verifyDistributionRoot(File distDir) {
        File dir = super.verifyDistributionRoot(distDir)
        if (!IS_WINDOWS) {
            copyNpmAndNpxToBin(dir)
        }
        dir
    }

    private void copyNpmAndNpxToBin(File destDir) {
        log.debug('Fixing npm & npx scripts in bin')
        projectOperations.copy { CopySpec spec ->
            spec.into new File(destDir, 'bin')
            spec.from new File(destDir, 'lib/node_modules/npm/bin/npx')
            spec.from new File(destDir, 'lib/node_modules/npm/bin/npm')
            spec.fileMode = 0755

            spec.filter { String line ->
                if (line.contains('$basedir/node_modules')) {
                    line.replace('node_modules', '../lib/node_modules')
                } else {
                    line
                }
            }
        }
    }
}

