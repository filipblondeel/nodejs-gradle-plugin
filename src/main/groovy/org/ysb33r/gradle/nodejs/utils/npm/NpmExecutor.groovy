/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.utils.npm

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Provider
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.NpmPackageDescriptor
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.gradle.nodejs.internal.npm.NpmExecSpecInstantiator
import org.ysb33r.gradle.nodejs.internal.npm.PackageJson
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON

/** Utility methods that aids in running NPM
 *
 * @since 0.5
 */
@CompileStatic
@Slf4j
class NpmExecutor {

    public static final String NPM_INSTALL = 'install'

    /**
     * Use this value to force the use of the bundled NPM rather than the version supplied by an NPM extension.
     * This is used with {@link #configureSpecFromExtensions} and in the constructor
     *
     * @since 0.12
     */
    public static final boolean FORCE_NODE_DEFAULT_FOR_NPM = true

    /**
     *
     * @param projectOperations Operations for the specific project context
     * @param nodeJS {@link NodeJSExtension} to use for resolving {@code node} location.
     * @param npm {@link NpmExtension} to use for resolving NPM-related activities.
     * @param forceNodeDefaultForNpm Whether to force the NPM to be used from the bundled NPM rather than
     *   the one supplied by the NPM extension. Default is to use the version supplied by the NPM extension.
     *
     * @since 0.10
     */
    NpmExecutor(
        ProjectOperations projectOperations,
        NodeJSExtension nodeJS,
        NpmExtension npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        this.projectOperations = projectOperations
        this.nodeJS = nodeJS
        this.npm = npm
        this.instantiator = new NpmExecSpecInstantiator(nodeJS, npm)
        this.forceNodeDefaultForNpm = forceNodeDefaultForNpm
    }

    /** Configures an NpmExecSpec from a NodeJSExtensions and a NpmExtension.
     *
     * <p> Will set {@code npm_config_userconfig} and {@code npm_config_globalconfig}
     *   environmental variables. The working directory is set it to the {@code homeDirectoryProperty}
     *   as defined by the {@link NpmExtension}.
     *
     * @param execSpec NPM execution spec that needs configuration.
     * @return Configured{@link NpmExecSpec}
     *
     * @since 0.10
     */
    NpmExecSpec configureSpecFromExtensions(NpmExecSpec execSpec) {
        configureSpecFromExtensions(execSpec, nodeJS, npm, forceNodeDefaultForNpm)
    }

    /** Runs NPM given a fully-configured execution specification.
     *
     * @param execSpec Specification to execute
     * @return Execution result
     * @throw May throw depending on whether execution specification was mal-configured or whether
     *   execution itself failed.
     *
     * @since 0.10
     */
    ExecResult runNpm(NpmExecSpec execSpec) {
        Closure runner = { NpmExecSpec fromSpec, ExecSpec toSpec ->
            fromSpec.copyToExecSpec(toSpec)
        }
        log.debug "NPM uses ${execSpec.workingDir}"
        log.debug "NPM environment: ${execSpec.environment}"
        projectOperations.exec runner.curry(execSpec)
    }

    /** Installs an NPM executable by running {@code npmExt install} in a controlled environment.
     *
     * @param npmPackageDescriptor Description of NPM executable.
     * @param installGroup Production, development of optional installation.
     * @param additionalArgs Any additional arguments that might be deemed necessary to customise the installation.
     *   This is here to provide flexibility as the author cannot foresee all common use cases.
     *   Should rarely be used. Should you find using this method regular occurance it might be prudent to
     *   to raise an issue to ask to a feature update and explaining the context in which this is needed.
     * @param additionalEnvironment Additional environmental settings when installing. This can be used to set the
     *   system path variable if need be.
     * @return List of files that were installed
     *
     * @sa https://docs.npmjs.com/cli/install
     *
     * @since 0.10
     */
    FileTree installNpmPackage(
        final NpmPackageDescriptor npmPackageDescriptor,
        final NpmDependencyGroup installGroup,
        Iterable<String> additionalArgs,
        Map<String, Object> additionalEnvironment = [:]
    ) {

        Map<String, Object> env = [:]
        env.putAll(additionalEnvironment)
        String pathVar = OperatingSystem.current().pathVar

        if (OperatingSystem.current().unix) {
            String shPath = OperatingSystem.current().findAllInPath('sh').first().parentFile.absolutePath
            if (env[pathVar]) {
                env[pathVar] = "${env[pathVar]}${OperatingSystem.current().pathSeparator}${shPath}"
            } else {
                env[pathVar] = shPath
            }
        } else if (OperatingSystem.current().windows) {
            String cmdPath = OperatingSystem.current().findAllInPath('cmd.exe').first().parentFile.absolutePath
            if (env[pathVar]) {
                env[pathVar] = "${env[pathVar]}${OperatingSystem.current().pathSeparator}${cmdPath}"
            } else {
                env[pathVar] = cmdPath
            }
        }

        NpmExecSpec execSpec = instantiator.create(projectOperations)
        execSpec.with {
            command(NPM_INSTALL)
            cmdArgs npmPackageDescriptor.toString()
            cmdArgs installGroup.saveArgument
            cmdArgs additionalArgs
            environment(env)
        }

        configureSpecFromExtensions(execSpec)
        runNpm(execSpec).assertNormalExitValue()

        String scope = npmPackageDescriptor.scope ? "@${npmPackageDescriptor.scope}/" : ''
        projectOperations.fileTree("${execSpec.workingDir}/node_modules/${scope}${npmPackageDescriptor.packageName}")
    }

    /** Installs packages from a {@code package.json} description.
     *
     * @param packageJson Path to a {@code package.json} file.
     * @param additionalArgs Additional arguments to be passed to NPM install command.
     * @param productionOnly Only install the production packages.
     * @return {@link org.gradle.api.file.FileTree} with lists of files that were installed.
     *   Will also include files from packages that were already there, but which would have been installed otherwise.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @since 0.10
     */
    FileTree installPackagesFromDescription(
        final File packageJson,
        Iterable<String> additionalArgs,
        boolean productionOnly = false
    ) {
        if (packageJson.name != PACKAGE_JSON || !packageJson.exists()) {
            throw new GradleException("${packageJson} does not exist or is not a valid description file")
        }

        if (packageJson.parentFile != npm.homeDirectory) {
            throw new GradleException("${packageJson} is not a child of ${npm.homeDirectory}")
        }

        NpmExecSpec execSpec = instantiator.create(projectOperations)
        execSpec.with {
            command(NPM_INSTALL)
            if (productionOnly) {
                cmdArgs('--production')
                cmdArgs(packageJson.parentFile.absolutePath)
                cmdArgs(additionalArgs)
            }
        }
        configureSpecFromExtensions(execSpec)
        runNpm(execSpec).assertNormalExitValue()
        calculateInstallableFiles(packageJson)
    }

    /** Works out where the installation folder will be for a executable.
     *
     * @param npmPackageDescriptor Description of NPM executable
     * @return The path where the executable will be installed to
     *
     * @since 0.10
     */
    File getPackageInstallationFolder(
        final NpmPackageDescriptor npmPackageDescriptor
    ) {
        NpmExecSpec execSpec = instantiator.create(projectOperations)
        configureSpecFromExtensions(execSpec)
        String scope = npmPackageDescriptor.scope ? "@${npmPackageDescriptor.scope}/" : ''
        new File("${execSpec.workingDir}/node_modules/${scope}${npmPackageDescriptor.packageName}")
    }

    /** Creates a template {@code package.json} file.
     *
     * @param projectName Name of the project.
     * @param projectVersion Version/tag of the project.
     * @return Location of the generated file.
     *
     * @since 0.10
     */
    File initPkgJson(
        final String projectName,
        final Provider<String> projectVersion
    ) {
        final String name = "\"name\": \"${projectName}\","
        final String version = "\"version\": \"${projectVersion.get()}\","
        final String description = "\"description\": \"${projectName}\","

        NpmExecSpec execSpec = instantiator.create(projectOperations)
        execSpec.with {
            command('init')
            cmdArgs('-f', '-s')
        }
        configureSpecFromExtensions(execSpec)
        runNpm(execSpec).assertNormalExitValue()

        File packageJson = new File(execSpec.workingDir, PACKAGE_JSON)

        if (!packageJson.exists()) {
            throw new GradleException("${packageJson.absolutePath} was not created as expected")
        }

        packageJson.text = packageJson.text.
            replaceAll(~/"name":\s+".+?",/, name).
            replaceAll(~/"version":\s+".+?",/, version).
            replaceAll(~/"description":\s+".+?",/, description)

        packageJson
    }

    /** Returns a live set of installable files.
     *
     * <p> This is an approximation. It parses the {@code package.json} file to discover dependencies, then
     *   recursively parses all other {@code package.json} files it find in those dependencies.
     *
     * @param rootPackageJson Initial package.json file to start traversal.
     * @return Live file collection, meaning it is possible to add more executable directories.
     *   Returns null if no dependencies, optional dependencies or dev dependencies were found.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @since 0.10
     */
    FileTree calculateInstallableFiles(File rootPackageJson) {
        if (rootPackageJson.name != PACKAGE_JSON || !rootPackageJson.exists()) {
            throw new GradleException("${rootPackageJson} does not exist or is not a valid description file")
        }

        if (rootPackageJson.parentFile != npm.homeDirectory) {
            throw new GradleException("${rootPackageJson} is not a child of ${npm.homeDirectory}")
        }

        PackageJson descriptor = PackageJson.parsePackageJson(rootPackageJson)
        Set<String> pkgNames = []
        pkgNames.addAll(descriptor.dependencies.keySet())
        pkgNames.addAll(descriptor.devDependencies.keySet())
        pkgNames.addAll(descriptor.optionalDependencies.keySet())
        if (pkgNames.empty) {
            return null
        }

        String root = new File(npm.homeDirectory, 'node_modules').absolutePath
        Set<String> pkgDirectories = Transform.toSet(pkgNames) { String name ->
            "${root}/${name}".toString()
        }

        FileTree tree = projectOperations.fileTree(projectOperations.files(pkgDirectories))
        for (String dir : pkgDirectories) {
            File nextPackageJson = new File(root, PACKAGE_JSON)
            if (nextPackageJson.exists()) {
                log.debug "Processing '${nextPackageJson}' for NPM dependencies"
                FileTree nextCollection = calculateInstallableFiles(nextPackageJson)
                if (nextCollection != null) {
                    tree += nextCollection
                }
            }
        }

        tree
    }

    /** Configures an NpmExecSpec from a NodeJSExtensions and a NpmExtension.
     *
     * <p> Will set {@code npm_config_userconfig} and {@code npm_config_globalconfig}
     *   environmental variables. The working directoryis set it to the {@code homeDirectoryProperty}
     *   as defined by the {@link NpmExtension}.
     *
     * @param execSpec NPM execution spec that needs configuration.
     * @param nodeJS NodeJSExtension
     * @param npm NpmExtension.
     * @param forceNodeDefaultForNpm Whether to force the NPM to be used from the bundled NPM rather than
     *   the one supplied by the NPM extension. Default is to use the version supplied by the NPM extension.
     * @return Configured{@link NpmExecSpec}
     *
     * @since 0.10
     */
    static NpmExecSpec configureSpecFromExtensions(
        NpmExecSpec execSpec,
        NodeJSExtension nodeJS,
        NpmExtension npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        execSpec.with {
            executable = forceNodeDefaultForNpm ? nodeJS.npmCliJsProvider : npm.executable
            workingDir = npm.homeDirectory
            environment(environmentFromExtensions(nodeJS, npm))
            nodeExecutable(nodeJS.executable)
        }
        execSpec
    }

    /**
     * Creates an environment based up on extensions.
     *
     * @param nodeJS NodeJSExtension.
     * @param npm NpmExtension.
     * @return Environment*
     * @since 0.10
     */
    static Map<String, Object> environmentFromExtensions(NodeJSExtension nodeJS, NpmExtension npm) {
        Map<String, Object> env = [:]
        env.putAll(nodeJS.environment)
        env.put('npm_config_userconfig', npm.localConfig.absolutePath)
        env.put('npm_config_globalconfig', npm.globalConfig.absolutePath)
        env
    }

    private final NodeJSExtension nodeJS
    private final NpmExtension npm
    private final ProjectOperations projectOperations
    private final NpmExecSpecInstantiator instantiator
    private final boolean forceNodeDefaultForNpm

    /** Creates a template {@code package.json} file.
     *
     * @param projectName Name of the project.
     * @param projectVersion Version/tag of the project.
     * @param project Project for which the {@package.json} file needs to be created.
     * @param nodeJSExtension Configured {@Link NodeJSExtension}.
     * @param npmExtension Configured {@link NpmExtension}
     * @return Location of the generated file.
     *
     * @deprecated
     *
     * @since 0.5
     */
    @Deprecated
    static File initPkgJson(
        final String projectName,
        final String projectVersion,
        final Project project,
        final NodeJSExtension nodeJSExtension,
        final NpmExtension npmExtension
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        new NpmExecutor(po, nodeJSExtension, npmExtension)
            .initPkgJson(projectName, project.provider { -> projectVersion })
    }

    /** Creates a template {@code package.json} file.
     *
     * @param project Project for which the {@package.json} file needs to be created.
     * @param nodeJSExtension Configured {@Link NodeJSExtension}.
     * @param npmExtension Configured {@link NpmExtension}
     * @return Location of the generated file.
     *
     * @deprecated
     */
    @Deprecated
    static File initPkgJson(
        final Project project,
        final NodeJSExtension nodeJSExtension,
        final NpmExtension npmExtension
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        new NpmExecutor(po, nodeJSExtension, npmExtension).initPkgJson(project.name, po.versionProvider)
    }

    /** Returns a live set of installable files.
     *
     * <p> This is an approximation. It parses the {@code package.json} file to discover dependencies, then
     *   recursively parses all other {@code package.json} files it find in those dependencies.
     *
     * @param project Gradle project this installation is associated with
     * @param npmExtension Extension that defines the NPM context.
     * @param rootPackageJson Initial package.json file to start traversal.
     * @return Live file collecton, meaning it is possible to add more executable directories.
     *   Returns null if no dependencies, optional dependencies or dev dependencies were found.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @deprecated
     */
    @Deprecated
    static FileTree calculateInstallableFiles(Project project, NpmExtension npmExtension, File rootPackageJson) {
        ProjectOperations po = ProjectOperations.create(project)
        def nodejs = project.extensions.getByType(NodeJSExtension)
        new NpmExecutor(po, nodejs, npmExtension).calculateInstallableFiles(rootPackageJson)
    }

    /** Works out where the installation folder will be for a executable.
     *
     * @param project Gradle {@link org.gradle.api.Project} that this installation is associated with.
     * @param nodeJSExtension A NodeJS project or task extension
     * @param npmExtension A NPM project or task extension
     * @param npmPackageDescriptor Description of NPM executable
     * @return The path where the executable will be installed to
     *
     * @deprecated
     */
    @Deprecated
    static File getPackageInstallationFolder(
        final Project project,
        final NodeJSExtension nodeJSExtension,
        final NpmExtension npmExtension,
        final NpmPackageDescriptor npmPackageDescriptor
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        new NpmExecutor(po, nodeJSExtension, npmExtension).getPackageInstallationFolder(npmPackageDescriptor)
    }

    /** Installs packages from a {@code package.json} description.
     *
     * @param project Project the installation is associated with.
     * @param nodeJSExtension A NodeJS project or task extension.
     * @param npmExtension A NPM project or task extension.
     * @param packageJson Path to a {@code package.json} file.
     * @param additionalArgs Additional arguments to be passed to NPM install command.
     * @param productionOnly Only install the production packages.
     * @return {@link org.gradle.api.file.FileTree} with lists of files that were installed.
     *   Will also include files from packages that were already there, but which would have been installed otherwise.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings('ParameterCount')
    static FileTree installPackagesFromDescription(
        final Project project,
        final NodeJSExtension nodeJSExtension,
        final NpmExtension npmExtension,
        final File packageJson,
        Iterable<String> additionalArgs,
        boolean productionOnly = false
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        new NpmExecutor(po, nodeJSExtension, npmExtension)
            .installPackagesFromDescription(packageJson, additionalArgs, productionOnly)
    }

    /** Installs packages from a {@code package.json} description.
     *
     * @param project Project the installation is associated with.
     * @param packageJson Path to a {@code package.json} file.
     * @param additionalArgs Additional arguments to be passed to NPM install command.
     * @return {@link org.gradle.api.file.FileTree} with lists of files that were installed.
     *   Will also include files from packages that were already there, but which would have been installed otherwise.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code project.npmExt.homeDirectoryProperty}.
     * @deprecated
     */
    @Deprecated
    static FileTree installPackagesFromDescription(
        final Project project,
        final File packageJson,
        Iterable<String> additionalArgs
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        def nodejs = project.extensions.getByType(NodeJSExtension)
        def npm = project.extensions.getByType(NpmExtension)
        new NpmExecutor(po, nodejs, npm)
            .installPackagesFromDescription(packageJson, additionalArgs)
    }

    /** Installs an NPM executable by running {@code npmExt install} in a controlled environment.
     *
     * @param project Gradle {@link org.gradle.api.Project} that this installation is associated with.
     * @param nodeJSExtension A NodeJS project or task extension.
     * @param npmExtension A NPM project or task extension.
     * @param npmPackageDescriptor Description of NPM executable.
     * @param installGroup Production, development of optional installation.
     * @param additionalArgs Any additional arguments that might be deemed necessary to customise the installation.
     *   This is here to provide flexibility as the auithro cannot foresee all common use cases.
     *   Should rarely be used. Should you find using this method regular occurance it might be prudent to
     *   to raise an issue to ask to a feature update and explaining the context in which this is needed.
     * @param additionalEnvironment Additional environmental settings when installing. This can be used to set the
     *   system path variable if need be.
     * @return List of files that were installed
     *
     * @sa https://docs.npmjs.com/cli/install
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings('ParameterCount')
    static FileTree installNpmPackage(
        final Project project,
        final NodeJSExtension nodeJSExtension,
        final NpmExtension npmExtension,
        final NpmPackageDescriptor npmPackageDescriptor,
        final NpmDependencyGroup installGroup,
        Iterable<String> additionalArgs,
        Map<String, Object> additionalEnvironment = [:]
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        new NpmExecutor(po, nodeJSExtension, npmExtension)
            .installNpmPackage(npmPackageDescriptor, installGroup, additionalArgs, additionalEnvironment)
    }

    /** Installs an NPM executable by running {@code npmExt install} i a controlled environment.
     *
     * <p> This uses the global {@code nodejs} and {@code npmExt} project extension to find defaults.
     *
     * @param project Gradle {@link org.gradle.api.Project} that this installation is associated with.
     * @param npmPackageDescriptor Description of NPM executable
     * @param installGroup Production, development of optional installation.
     * @param additionalArgs Any additional arguments that might be deemed necessary to customise the installation.
     *   This is here to provide flexibility as the author cannot foresee all common use cases.
     *   Should rarely be used. Should you find using this method regular occurance it might be prudent to
     *   to raise an issue to ask to a feature update and explaining the context in which this is needed.
     * @return List of files that were installed
     *
     * @sa https://docs.npmjs.com/cli/install
     *
     * @deprecated
     */
    @Deprecated
    static FileTree installNpmPackage(
        final Project project,
        final NpmPackageDescriptor npmPackageDescriptor,
        final NpmDependencyGroup installGroup,
        Iterable<String> additionalArgs
    ) {
        ProjectOperations po = ProjectOperations.create(project)
        def nodejs = project.extensions.getByType(NodeJSExtension)
        def npm = project.extensions.getByType(NpmExtension)
        new NpmExecutor(po, nodejs, npm).installNpmPackage(npmPackageDescriptor, installGroup, additionalArgs)
    }

    /** Runs NPM given a fully-configured execution specification.
     *
     * @param project The project in which context this execution will be performed.
     * @param execSpec
     * @return Execution result
     * @throw May throw depending on whether execution specification was mal-configured or whether
     *   execution itself failed.
     * @deprecated
     */
    @Deprecated
    static ExecResult runNpm(Project project, NpmExecSpec execSpec) {
        ProjectOperations po = ProjectOperations.create(project)
        def nodejs = project.extensions.getByType(NodeJSExtension)
        def npm = project.extensions.getByType(NpmExtension)
        new NpmExecutor(po, nodejs, npm).runNpm(execSpec)
    }
}
