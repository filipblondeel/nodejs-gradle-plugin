/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification
import spock.lang.IgnoreIf
import spock.lang.Timeout
import spock.lang.Unroll
import spock.util.environment.RestoreSystemProperties

import static org.ysb33r.gradle.nodejs.functionaltest.internal.CustomNpmInstallerSpec.NPM_TEST_VERSION
import static org.ysb33r.gradle.nodejs.plugins.WrapperPlugin.WRAPPER_TASK_NAME
import static org.ysb33r.gradle.nodejs.tasks.NodeBinariesCacheTask.PROPERTIES_FILENAME

@IgnoreIf({ SKIP_TESTS })
@RestoreSystemProperties
class NodeWrappersSpec extends NpmIntegrationTestSpecification {

    public static final List<String> ENV_VARS = OS.windows ? [
        'DEBUG=1',
        "TEMP=${System.getenv('TEMP')}",
        "${OS.pathVar}=${System.getenv('Path')}"
    ] : [
        'DEBUG=1',
        "PATH=${System.getenv('PATH')}"
    ]
    public static final Set<String> SCRIPTS = ['nodew', 'npmw', 'npxw']

    Map<String, File> wrappers = [:]

    void setup() {
        wrapperPaths(projectDir, wrappers)
    }

    @Timeout(300)
    @Unroll
    void 'Create wrappers #description'() {
        setup:
        writeBuildFile(withNpmDevPlugin)
        def paths = withNpmDevPlugin ? wrapperPaths(new File(projectDir, 'src/node'), [:]) : wrappers

        when: 'The nodejs wrapper task is executed'
        BuildResult result = wrapperRunner.build()
        def gwRelPathEntry = paths['nodew'].readLines().find { it.startsWith('GRADLE_WRAPPER_RELATIVE_PATH') }
        def dotGradleRelPathEntry = paths['nodew'].readLines().find { it.startsWith('DOT_GRADLE_RELATIVE_PATH') }

        then: 'nodejs wrapper scripts are generated'
        result.task(":${WRAPPER_TASK_NAME}").outcome == TaskOutcome.SUCCESS
        paths.values()*.exists().every { it }
        gwRelPathEntry.endsWith("=${getEscapedPathString(gwRelPath)}")
        dotGradleRelPathEntry.endsWith("=${getEscapedPathString(dotGradleRelPath)}")

        where:
        description         | withNpmDevPlugin | gwRelPath | dotGradleRelPath
        'at projectDir'     | false            | '.'       | '.gradle'
        'with NpmDevPlugin' | true             | '../..'   | '../../.gradle'
    }

    @Timeout(300)
    @Unroll
    void 'Run #wrapper.cmd'() {
        setup: 'wrappers have been created'
        writeBuildFile()
        wrapperRunner.build()

        when: 'The node wrapper script is executed'
        Result runResult = runWrapper(new File(projectDir, wrapper.cmd), wrapper.instruction)

        then: 'It should print out the help message'
        runResult.exitCode == wrapper.exit
        runResult.out.contains(wrapper.containedText)

        where:
        wrapper << [
            [
                cmd          : scriptName('nodew'),
                instruction  : '--help',
                containedText: 'NODE_DEBUG',
                exit         : 0
            ],
            [
                cmd          : scriptName('npmw'),
                instruction  : 'version',
                containedText: 'npm:',
                exit         : 0
            ],
            [
                cmd          : scriptName('npxw'),
                instruction  : '--help',
                containedText: 'npm exec',
                exit         : 0
            ]
        ]
    }

    @Unroll
    void 'Update workspace.xml for Idea #text'() {
        setup:
        writeBuildFileForIdea(custom)
        new File(projectDir, 'src/node').mkdirs()

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, ['idea', '-i', '-s']).build()

        then:
        new File(projectDir, "${projectDir.name}.iws").text.contains('nodew')

        where:
        text           | custom
        'standard npm' | false
        'custom npm'   | true
    }

    static class Result {
        int exitCode
        String out
    }

    Result runWrapper(File wrapper, String args) {
        StringWriter err = new StringWriter()
        StringWriter out = new StringWriter()
        Process runWrapper = "${wrapper.absolutePath} ${args}".execute(ENV_VARS, wrapper.parentFile)
        runWrapper.consumeProcessOutput(out, err)
        runWrapper.waitForOrKill(1000)
        int exitCode = runWrapper.exitValue()
        if (exitCode) {
            println '---[ stderr ]----------------------------------'
            println err
            println '---[ stdout ]----------------------------------'
            println out
            println '---[ node-wrapper ]----------------------------'
            println new File(projectCacheDir, PROPERTIES_FILENAME).text
            println '-----------------------------------------------'
        }
        new Result(exitCode: exitCode, out: out.toString())
    }

    GradleRunner getWrapperRunner() {
        getGradleRunner(
            IS_GROOVY_DSL,
            projectDir,
            [
                'wrapper',
                WRAPPER_TASK_NAME,
                '-i',
                '-s'
            ]
        )
    }

    void writeBuildFile(boolean withNpmDevPlugin = false) {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.wrapper'
            ${withNpmDevPlugin ? "id 'org.ysb33r.nodejs.dev'" : ''}
        }
        """
    }

    void writeBuildFileForIdea(boolean withCustomNpm = false) {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.wrapper'
            id 'org.ysb33r.nodejs.dev'
            id 'idea'
        }
        """

        if (withCustomNpm) {
            buildFile << """
            npm.executableByVersion('${NPM_TEST_VERSION}')
            """
        }
    }

    Map<String, File> wrapperPaths(File baseDir, Map<String, File> wp) {
        SCRIPTS.each {
            wp.putAll([
                (it)                    : new File(baseDir, it),
                ("${it}_bat".toString()): new File(baseDir, "${it}.bat")
            ])
        }
        wp
    }

    private static String scriptName(String baseName) {
        OS.windows ? "${baseName}.bat" : baseName
    }
}