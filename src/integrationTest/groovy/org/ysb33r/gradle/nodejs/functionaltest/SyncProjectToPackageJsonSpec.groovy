/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification
import spock.lang.Unroll

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON

class SyncProjectToPackageJsonSpec extends NpmIntegrationTestSpecification {

    void 'Sync dependencies to package.json'() {
        setup:
        File packageJson = new File(projectDir, PACKAGE_JSON)
        def args = ['-i', '-s', 'syncPackageJson']
        buildFile << """
        version = '1.2.3'
        ext.antoraVersion = '2.2'
        dependencies {
            npm npmPackage(scope: 'antora', name: 'cli', tag: antoraVersion, type: 'dev')
            npm npmPackage(scope: 'antora', name: 'site-generator-default', tag: antoraVersion, type: 'dev')
        }

        task initPackageJson( type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInit ) {
            nodejs {
                useSystemPath()
            }
        }
        task syncPackageJson(type: org.ysb33r.gradle.nodejs.tasks.SyncProjectToPackageJson) {
            dependsOn initPackageJson
            forceSemver = false
            forceTwoSpaceIndent = true
            sortOutput = true
            packageJsonVersion = '1.2.4'
            packageJsonFile = initPackageJson.packageJsonFileProvider
            npmConfigurations 'npm'
        }
        """

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).build()
        String contents = packageJson.text
        def matcher = contents =~ /(?s).+?\s+"name": "junit\d+?",\s+"version": "1.2.4",\s+"description": "".+/

        then:
        contents.contains('"@antora/cli": "2.2"')
        matcher.matches()

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args + ['--rerun-tasks']).build()

        then:
        noExceptionThrown()
    }

    @Unroll
    void 'Load correct version of project on Gradle #ver'() {
        setup:
        File packageJson = new File(projectDir, "src/node/${PACKAGE_JSON}")
        def args = ['-i', '-s', 'packageJsonInstall', 'syncProjectToPackageJson']

        packageJson.parentFile.mkdirs()
        packageJson.text = '''{
            "name": "5.3.1",
            "version": "0.0.0",
            "license": "ISC",
            "devDependencies": {
                "@antora/cli": "2.2",
                "@antora/site-generator-default": "2.2"
            },
            "main": "app/index.js",
            "scripts": {
                "test": "npx --help > test.txt"
            }
        }
        '''
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.dev'
            id 'org.ysb33r.nodejs.wrapper'
        }
    
        version = '1.0.0'
        
        nodejs {
            executableByVersion('${NODEJS_VERSION}')
            useSystemPath()
        }
    
        syncProjectToPackageJson {
            forceTwoSpaceIndent = true
            sortOutput = true
        }
        """
        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).withGradleVersion(ver).build()

        then:
        noExceptionThrown()

        where:
        ver << ['4.9', '6.9']
    }

}